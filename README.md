# dv_cert

Terraform Module for: Domain Validation (DV) TLS Cert for a domain

Similar to how "Let's Encrypt" uses ACME to validate, we use AWS APIs instead

Before AWS will give us a DV certificate, we will prove ownership & control
of the domain by creating an AWS defined DNS record

# WARNING
AWS certificate (de)allocation is VERY SLOW

# Example
Functional Example used by the CI can be found in [example/main.tf](example/main.tf)

### Sample

     # BTW: DON'T PIN TO MASTER, pick a tag so you're code doesn't break if if the interface changes

     module "dv_cert" {
       source      = "git::https://gitlab.com/jasondearte/tf.dv_cert.git?ref=master"
       root_domain = "example.com"
     }


# Interface


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| record_ttl | TTL for the new r53 cname record used to verify domain ownership, in seconds | string | `3600` | no |
| root_domain | base domain that needs the certificate: example.com | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| arn | arn of the certificate |
| validation_url | obtusely long url that proves that we control the domain |

# Terraform Version
Terraform v0.11.8

