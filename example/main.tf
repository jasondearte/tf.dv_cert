provider "aws" {
  region = "us-east-1"
}

module "dv_cert" {
  source      = ".."
  root_domain = "mergedamage.com" #change to a domain you own
}

output "result" {
  value = {
    arn = "${module.dv_cert.arn}"
    url = "${module.dv_cert.validation_url}"
  }
}
