/**
* # dv_cert
*
* Terraform Module for: Domain Validation (DV) TLS Cert for a domain
*
* Similar to how "Let's Encrypt" uses ACME to validate, we use AWS APIs instead
*
* Before AWS will give us a DV certificate, we will prove ownership & control
* of the domain by creating an AWS defined DNS record
*
* # WARNING
* AWS certificate (de)allocation is VERY SLOW
*
* # Example
* Functional Example used by the CI can be found in [example/main.tf](example/main.tf)
*
* ### Sample
*
*      # BTW: DON'T PIN TO MASTER, pick a tag so you're code doesn't break if if the interface changes
*
*      module "dv_cert" {
*        source      = "git::https://gitlab.com/jasondearte/tf.dv_cert.git?ref=master"
*        root_domain = "example.com"
*      }
*
*
* # Interface
*/

data "aws_route53_zone" "zone" {
  name         = "${var.root_domain}."
  private_zone = false
}

resource "aws_route53_record" "cert" {
  name    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.zone.id}"
  records = ["${aws_acm_certificate.cert.domain_validation_options.0.resource_record_value}"]
  ttl     = "${var.record_ttl}"
}

resource "aws_acm_certificate" "cert" {
  domain_name               = "*.${var.root_domain}"
  subject_alternative_names = ["${var.root_domain}"]
  validation_method         = "DNS"
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = "${aws_acm_certificate.cert.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert.fqdn}"]
}
