#!/usr/bin/env bash

make_readme() {
    MODULE=$1

    echo "Making the readme"
    terraform fmt $MODULE

    terraform-docs md $MODULE   > $MODULE/README.md
    echo "# Terraform Version" >> $MODULE/README.md
    terraform --version        >> $MODULE/README.md
}

make_readme "."
