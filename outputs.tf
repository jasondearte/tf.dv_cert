output "arn" {
  description = "arn of the certificate"
  value       = "${aws_acm_certificate.cert.arn}"
}

output "validation_url" {
  description = "obtusely long url that proves that we control the domain"
  value       = "${aws_route53_record.cert.fqdn}"
}
