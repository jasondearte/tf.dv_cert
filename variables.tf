variable "root_domain" {
  type        = "string"
  description = "base domain that needs the certificate: example.com"
}

variable "record_ttl" {
  type        = "string"
  description = "TTL for the new r53 cname record used to verify domain ownership, in seconds"
  default     = "3600"
}
